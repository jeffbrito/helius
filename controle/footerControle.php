<?php
require_once("modelo/Contatos.php");
require_once("modelo/Redes.php");
require_once("modelo/Endereco.php");
require_once("admin/controle/Conect.php");
class FooterControle{
    //read
    //contatos
    function SelectContatos(){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM contatos;");
            $cmd->execute();
            $oia = $cmd->fetchAll(PDO::FETCH_CLASS, "Contatos");
            return $oia;
        }catch(PDOException $e){
            echo "Erro no pdo:{$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
            return false;
        }
        return true;
    }
    //updateContatos
    function updateContatos($id,$contatos){
        try{
            $conexao = new Conexao();
            $con1 = $contatos->getCont1();
            $con2 = $contatos->getCont2();
            $con3 = $contatos->getCont3();
            $cmd = $conexao->getConexao()->prepare("UPDATE contatos SET  cont1= :c1,cont2 = :c2 , cont3= :c3 WHERE id =:id;");
            $cmd->bindParam("id",$id);
            $cmd->bindParam("c1",$con1);
            $cmd->bindParam("c2",$con2);
            $cmd->bindParam("c3",$con3);
            if($cmd->execute()){
                $conexao->fecharConexao();
                return true;
            }else{
                $conexao->fecharConexao();
                return false;
            }
        }catch(PDOException $e){
            echo "Erro em pdo:{$e->getMessage()}";
        }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
        }
    }
    
    //read
    //redes
    function selectRedes(){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM redes;");
            $cmd->execute();
            $oia = $cmd->fetchAll(PDO::FETCH_CLASS, "Redes");
            return $oia;
        }catch(PDOException $e){
            echo "Erro no pdo:{$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
            return false;
        }
        return true;
    }
    function selectEndereco(){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM enderecos;");
            $cmd->execute();
            $oia = $cmd->fetchAll(PDO::FETCH_CLASS, "Endereco");
            return $oia;
        }catch(PDOException $e){
            echo "Erro no pdo:{$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
            return false;
        }
        return true;
    }
}
?>