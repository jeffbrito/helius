<?php
require_once("admin/controle/Conect.php");
require_once("modelo/Base.php");
class BaseControle{
   
    function selectIdBase($id){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM baseSite WHERE id = :id");
            $cmd->bindParam("id",$id);
            $cmd->execute();
            $result = $cmd->fetchAll(PDO::FETCH_CLASS, "Base");
            return $result;
        }catch(PDOException $e){
            echo "Erro no pdo:{$e->getMessage()}";
        }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
        }
    }
    function selectBaseMt(){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM baseSite;");
            $cmd->execute();
            $oia = $cmd->fetchAll(PDO::FETCH_CLASS, "Base");
            return $oia;
        }catch(PDOException $e){
            echo "Erro no pdo:{$e->getMessage()}";
            return false;
        }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
            return false;
        }
        return true;
    }
    function updateBase($id,$base){
        try{
            $conexao = new Conexao();
            $ti = $base->getTitle();
            $tex = $base->getTexto();
            $cmd = $conexao->getConexao()->prepare("UPDATE baseSite SET title = :ti,texto = :txt WHERE id =:id;");
            $cmd->bindParam("id",$id);
            $cmd->bindParam("txt",$tex);
            $cmd->bindParam("ti",$ti);
            if($cmd->execute()){
                $conexao->fecharConexao();
                return true;
            }else{
                $conexao->fecharConexao();
                return false;
            }
        }catch(PDOException $e){
            echo "Erro em pdo:{$e->getMessage()}";
        }catch(Exception $e){
            echo "Erro geral:{$e->getMessage()}";
        }
    }
    
    
}



?>