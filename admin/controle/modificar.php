<?php
session_start();
require_once("AdminControle.php");
try{
    $admin = new Admin();
    $admin->setNome($_POST['nome']);
    $admin->setEmail($_POST['email']);
    $admin->setUser($_POST['user']);
    $admin->setSenha($_POST['senha']);
    $control = new AdminControle();
    if ($control->update($_SESSION['id'],$admin)){
        header("Location:../visual/settings.php");
    }else{
        echo "erro";
    }
}catch(PDOException $e){
    echo "Erro de pdo:{$e->getMessage()}";  
}catch(Exception $e){
    echo "Erro geral:{$e->getMessage()}";
}


?>