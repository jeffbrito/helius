<?php

require_once("Conect.php");
require_once("../modelo/Admin.php");

class AdminControle{

	//criar
	function inserir($admin){
		try{
	      	    $conexao = new Conexao();
		    $nome = $admin->getNome();
		    $email = $admin->getEmail();
		    $user = $admin->getUser();
		    $senha = $admin->getSenha();
		    $query = $conexao->getConexao()->prepare("INSERT INTO admin(nome,email,user,senha) VALUES(:n,:e,:u,:se);");
		    $query->bindParam("n",$nome);
		    $query->bindParam("e",$email);
		    $query->bindParam("u",$user);
		    $query->bindParam("se",$senha);
		    if($query->execute()){
	   		$conexao->fecharConexao();
			return true;
		    }else{
			$conexao->fecharConexao();
			return false;
		    }
		    }catch(PDOException $e){
			    echo "Erro em pdo:{$e->getMessage()}";
			    return false;
		    }catch(Exception $e){
			    echo "Erro geral:{$e->getMessage()}";
			    return false;
		 }
	}

	function select(){
		try{
		   $conexao = new Conexao();
		   $cmd = $conexao->getConexao()->prepare("SELECT * FROM admin;");
		   $cmd->execute();
		   $oia = $cmd->fetchAll(PDO::FETCH_CLASS, "Admin");
		   return $oia;
		}catch(PDOException $e){
			echo "Erro no pdo:{$e->getMessage()}";
			return false;
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
			return false;
		}
		return true;
	}
	function selectIdnu($u,$s){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT id FROM admin WHERE user = :u AND senha = :s;");
	        $cmd->bindParam("u",$u);
	        $cmd->bindParam("s",$s);
	        $cmd->execute();
	        $result = $cmd->fetchAll(PDO::FETCH_CLASS,"Admin");
	        return $result;
	    }catch(PDOException $e){
	        echo "Erro no pdo:{$e->getMessage()}";
	    }catch(Exception $e){
	        echo "Erro geral:{$e->getMessage()}";
	    }
	    
	    
	}
	function selectId($id){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT id,nome,email,user,senha FROM admin WHERE id=:id");
	        $cmd->bindParam("id",$id);
	        $cmd->execute();
	        $result = $cmd->fetchAll(PDO::FETCH_CLASS, "Admin");
	        return $result;
	    }catch(PDOException $e){
	        echo "Erro no pdo:{$e->getMessage()}";
	    }catch(Exception $e){
	        echo "Erro geral:{$e->getMessage()}";
	    }
	}
	function update($id,$admin){
		try{
		   $conexao = new Conexao();
		   $nome = $admin->getNome();
		   $email = $admin->getEmail();
		   $user = $admin->getUser();
		   $senha = $admin->getSenha();
		   $cmd = $conexao->getConexao()->prepare("UPDATE admin SET nome = :n,email = :e, user = :u, senha = :se WHERE id =:id;");
		   $cmd->bindParam("id",$id);
		   $cmd->bindParam("n",$nome);
		   $cmd->bindParam("e",$email);
		   $cmd->bindParam("u",$user);
		   $cmd->bindParam("se",$senha);
		   if($cmd->execute()){		   	
 			  $conexao->fecharConexao();
			  return true;
		   }else{
			  $conexao->fecharConexao();
			  return false;
		   }
		}catch(PDOException $e){
			echo "Erro em pdo:{$e->getMessage()}";
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
		}
	}
	function remover($id){
		try{
			$conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("DELETE FROM admin WHERE id=:id");
			$cmd->bindParam("id",$id);
			if($cmd->execute()){
			    $conexao->fecharConexao();
			    return true;
			   
			}else{
			    $conexao->fecharConexao();
			    return false;
			}	
		}catch(PDOException $e){
			echo "Erro de pdo:{$e->getMessage()}";
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
		}
	
	}
	function selectUeS($u,$s){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT * FROM admin WHERE user = :u AND senha = :s;");
	        $cmd->bindParam("u",$u);
	        $cmd->bindParam("s",$s);
	        if($cmd->execute()){
	            if($cmd->rowCount() == 1){
	                return true;
	            }else{
	                return false;
	            }
	        }
	    }catch(PDOException $e){
	        echo "Erro em pdo:{$e->getMessage()}";
	    }catch(Exception $e){
	        echo "Erro geral:{$e->getMessage()}";
	    }
	    
	}
	function selectIdLog($u,$s){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT id FROM admin WHERE user = :u AND senha = :s;");
	        $cmd->bindParam("u",$u);
	        $cmd->bindParam("s",$s);
	        if($cmd->execute()){
	            if($cmd->rowCount() == 1){
	                return true;
	            }else{
	                return false;
	            }
	        }
	    }catch(PDOException $e){
	        echo "Erro em pdo:{$e->getMessage()}";
	    }catch(Exception $e){
	        echo "Erro geral:{$e->getMessage()}";
	    }
	    
	} 
}

?>
