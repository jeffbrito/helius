<?php
class Conexao{
    private $conexao;
    function __construct(){
        $host = "localhost"; 
        $user =  "root";
        $pwd = "123456"; 
        $bd = "helio"; 
        try{
            $this->setConexao(new PDO("mysql:host={$host};dbname={$bd}",$user,$pwd));
	    $this->getConexao()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	   }catch(PDOException $ex){
		    echo "<p>Erro em PDO: {$ex->getMessage()}</p>";
       	   }catch(Exception $ex){
		    echo "<p>Erro geral do sistema: {$ex->getMessage()}</p>";
	   }
	}
    function fecharConexao(){
	$this->conexao = NULL;
    }
    function getConexao(){ 
        return $this->conexao;
    }
    function setConexao($data){ 
        $this->conexao = $data;
    }
}
?>
