<?php
  class Admin{
    private $id;
	private $nome;
	private $email;
	private $user;
	private $senha;
	
	//getters
	public function getId(){
		return $this->id;
	}
	public function getNome(){
		return $this->nome;
	} 
	
	public function getEmail(){
		return $this->email;
	}
	public function getUser(){
		return $this->user;
	}
	public function getSenha(){
		return $this->senha;
	}
	//setters
	public function setId($i){
		$this->id = $i;
	}
	public function setNome($n){
		$this->nome = $n;
	}
	
	public function setEmail($e){
		$this->email = $e;
	}
	public function setUser($u){
		$this->user = $u;
	}
	public function setSenha($se){
		$this->senha = $se;
	}
     }
?>
