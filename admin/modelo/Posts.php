<?php

	/**
	 * @author jeff
	 *
	 */
	class Posts{

		private $id;
		private $postTitulo;
		private $postSubtitulo;
		private $postTexto;
		private $postDia;
		private $postMes;
		private $postAno;
		private $postHora;
		private $postMinuto;
		private $postSegundo;
		private $postImgNome;
		private $postImgType;
		private $postImgTmp;

		//metodos getters e setters

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }
    
        /**
         * @return mixed
         */
        public function getPostTitulo()
        {
            return $this->postTitulo;
        }
    
        /**
         * @return mixed
         */
        public function getPostSubtitulo()
        {
            return $this->postSubtitulo;
        }
    
        /**
         * @return mixed
         */
        public function getPostTexto()
        {
            return $this->postTexto;
        }
    
        /**
         * @return mixed
         */
        public function getPostDia()
        {
            return $this->postDia;
        }
    
        /**
         * @return mixed
         */
        public function getPostMes()
        {
            return $this->postMes;
        }
    
        /**
         * @return mixed
         */
        public function getPostAno()
        {
            return $this->postAno;
        }
    
        /**
         * @return mixed
         */
        public function getPostHora()
        {
            return $this->postHora;
        }
    
        /**
         * @return mixed
         */
        public function getPostMinuto()
        {
            return $this->postMinuto;
        }
    
        /**
         * @return mixed
         */
        public function getPostSegundo()
        {
            return $this->postSegundo;
        }
    
        /**
         * @return mixed
         */
        public function getPostImgNome()
        {
            return $this->postImgNome;
        }
    
        /**
         * @return mixed
         */
        public function getPostImgType()
        {
            return $this->postImgType;
        }
    
        /**
         * @return mixed
         */
        public function getPostImgTmp()
        {
            return $this->postImgTmp;
        }
    
        /**
         * @param mixed $id
         */
        public function setId($id)
        {
            $this->id = $id;
        }
    
    
        /**
         * @param mixed $postTitulo
         */
        public function setPostTitulo($postTitulo)
        {
            $this->postTitulo = $postTitulo;
        }
    
        /**
         * @param mixed $postSubtitulo
         */
        public function setPostSubtitulo($postSubtitulo)
        {
            $this->postSubtitulo = $postSubtitulo;
        }
    
        /**
         * @param mixed $postTexto
         */
        public function setPostTexto($postTexto)
        {
            $this->postTexto = $postTexto;
        }
    
        /**
         * @param mixed $postDia
         */
        public function setPostDia($postDia)
        {
            $this->postDia = $postDia;
        }
    
        /**
         * @param mixed $postMes
         */
        public function setPostMes($postMes)
        {
            $this->postMes = $postMes;
        }
    
        /**
         * @param mixed $postAno
         */
        public function setPostAno($postAno)
        {
            $this->postAno = $postAno;
        }
    
        /**
         * @param mixed $postHora
         */
        public function setPostHora($postHora)
        {
            $this->postHora = $postHora;
        }
    
        /**
         * @param mixed $postMinuto
         */
        public function setPostMinuto($postMinuto)
        {
            $this->postMinuto = $postMinuto;
        }
    
        /**
         * @param mixed $postSegundo
         */
        public function setPostSegundo($postSegundo)
        {
            $this->postSegundo = $postSegundo;
        }
    
        /**
         * @param mixed $postImgNome
         */
        public function setPostImgNome($postImgNome)
        {
            $this->postImgNome = $postImgNome;
        }
    
        /**
         * @param mixed $postImgType
         */
        public function setPostImgType($postImgType)
        {
            $this->postImgType = $postImgType;
        }
    
        /**
         * @param mixed $postImgTmp
         */
        public function setPostImgTmp($postImgTmp)
        {
            $this->postImgTmp = $postImgTmp;
        }
    		
		
		
	}
?>
