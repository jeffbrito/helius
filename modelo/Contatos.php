<?php

class Contatos{
    private $id;
    private $cont1;
    private $cont2;
    private $cont3;    
    //getters

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCont1()
    {
        return $this->cont1;
    }

    /**
     * @return mixed
     */
    public function getCont2()
    {
        return $this->cont2;
    }

    /**
     * @return mixed
     */
    public function getCont3()
    {
        return $this->cont3;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $cont1
     */
    public function setCont1($cont1)
    {
        $this->cont1 = $cont1;
    }

    /**
     * @param mixed $cont2
     */
    public function setCont2($cont2)
    {
        $this->cont2 = $cont2;
    }

    /**
     * @param mixed $cont3
     */
    public function setCont3($cont3)
    {
        $this->cont3 = $cont3;
    }
    
}

    
?>